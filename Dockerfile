FROM ruby:2.3-alpine
MAINTAINER Dylan Guedes <dguedes@ime.usp.br>

ENV BUILD_PACKAGES="ruby-dev build-base bash" \
    DEV_PACKAGES="zlib-dev libxml2-dev libxslt-dev tzdata yaml-dev sqlite-dev postgresql-dev" \
    RUBY_PACKAGES="ruby-json yaml nodejs"

RUN apk update && \
    apk upgrade && \
    apk add --update\
    $BUILD_PACKAGES \
    $DEV_PACKAGES \
    $RUBY_PACKAGES && \
    rm -rf /var/cache/apk/* && \
    mkdir -p /app/

WORKDIR /app/
COPY . /app/
EXPOSE 3000
RUN bundle config build.nokogiri --use-system-libraries && \
            bundle install && \
            bundle clean

CMD ["bundle", "exec", "rails", "s", "-p", "3000", "-b", "0.0.0.0"]
